'use strict'

class DB {
	constructor(){
		if (!DB.dbInstance) { DB.dbInstance = this }

		return DB.dbInstance;
	}
	save(data) {
		localStorage.setItem('db', JSON.stringify(data))
	}
	get() {
		return JSON.parse(localStorage.getItem('db')) || null
	}
	remove() {
		localStorage.removeItem('db')
	}
}
