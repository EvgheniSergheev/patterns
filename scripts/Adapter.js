'use strict'

class UserRegister {
	constructor(name, email) {
		this.name = name
		this.email = email
	}
}

class UserRegisterAdapter extends UserRegister {
	register() {
		let jsonFile = {
				firstName: this.name.split(' ')[0],
				lastName: this.name.split(' ')[1],
				eMail: this.email
			}

		console.log(jsonFile)
	}
}