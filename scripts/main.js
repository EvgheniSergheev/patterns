'use strict'


const dbInstance = new DB();
const dbInstance2 = new DB();
console.log(dbInstance === dbInstance2);

const fileSaverInstance = new FileSaver()


let form = document.getElementById('form')

form.addEventListener('submit', (e) => {
	e.preventDefault()
	let name = document.getElementById('name').value,
		email = document.getElementById('email').value,
		storage = document.getElementById('storage').value

	const chooseStrategyInstance = new Strategy(storage)
	chooseStrategyInstance.enableStrategy(name, email)

	const userInstance = new UserRegisterAdapter(name, email)
	userInstance.register()
})