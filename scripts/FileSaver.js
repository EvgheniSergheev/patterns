'use strict'

class FileSaver {
	constructor() {

	}
	save({name, email}) {
		let link = document.createElement('a')

		link.href = URL.createObjectURL(new Blob([`Name: ${name} \nEmail: ${email}`], {type: 'type/text'}))
		link.setAttribute('download', 'userData.txt');
		document.body.appendChild(link);
		link.click()
		link.remove()
	}
}