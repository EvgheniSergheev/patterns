'use strict'

class Strategy {
	constructor(storage) {
		switch (parseFloat(storage)) {
			case 0:
				this.strategy = new DB()
				break;
			case 1:
				this.strategy = new FileSaver()
				break;
		}
	}
	enableStrategy(name, email) {
		this.strategy.save({name, email})
	}
}


